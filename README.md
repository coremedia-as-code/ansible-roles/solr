# CoreMedia - solr

Installs and configures a Solr service prepared for CoreMedia.

It is possible to configure both a *solr-master* and a *solr-slave*.



## Requirement

- java (8 or 11) (e.g. [corretto](https://github.com/bodsch/ansible-corretto-java))
- [coremedia](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/coremedia)


## supported and tested distributions

- CentOS 7 / 8
- debian 9 / 10
- ubuntu 18.04


## config parameters

```
solr_version: '7.7.2'

solr_download_url: "http://archive.apache.org/dist/lucene/solr/{{ solr_version }}/solr-{{ solr_version }}.tgz"

solr_group: coremedia


solr_host_name: "{{ ansible_fqdn }}"
solr_timezone: UTC

solr_java_home: /usr/lib/jvm/java
solr_heap_memory: 512 # in MB

solr_home_directory: /opt/coremedia/solr-home
solr_data_directory: /var/coremedia/solr-data
solr_pid_directory: /run/solr
solr_application_directory: /opt/solr
solr_log_directory: /var/log/solr

solr_port: 40080
solr_bind_ip: "127.0.0.1"

# valid values: ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF.
# default is INFO
solr_log_level: WARN

solr_jmx_port: 40099
solr_jmx_enable: true

# solr_master: true
solr_master_enabled: true
# solr_slave: false
solr_slave_enabled: false
solr_slave_master_url: http://solr-master.cm.local:40080/solr

solr_java_memory:
  - -Xmx{{ solr_heap_memory }}m

solr_ssl: false

solr_remove_cruft: true

```

## example

### solr-master

```
- hosts: all
  roles:
    - role: corretto
    - role: solr
      vars:
         solr_heap_memory: 1024
```



### solr-slave

```
- hosts: all
  roles:
    - role: corretto
    - role: solr
      vars:
        solr_heap_memory: 1024
        solr_slave_enabled: true
        solr_slave_master_url: http://solr-master:40080/solr
```

## tests

```
$ tox -e py36-ansible29 -- molecule test -s solr-master

$ tox -e py36-ansible29 -- molecule test -s solr-slave
```




