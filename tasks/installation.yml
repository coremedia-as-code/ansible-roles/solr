---

- name: check for unpacked solr
  stat:
    path: "{{ solr_application_directory }}/server"
  register: _solr_unpack

- block:
    - name: get checksum for solr-{{ solr_version }}
      become: false
      get_url:
        url: "{{ solr_download_url }}.sha512"
        dest: "{{ local_tmp_directory }}/solr-{{ solr_version }}.tgz.sha512"
      register: _download_checksum
      until: _download_checksum is succeeded
      retries: 5
      delay: 0
      # run_once: true
      delegate_to: localhost
      check_mode: false

    - name: extract checksum
      set_fact:
        solr_checksum: "{{ solr_checksums[0].split()[0] }}"
      vars:
        solr_checksums: '{{ lookup("file", "{{ local_tmp_directory }}/solr-{{ solr_version }}.tgz.sha512").splitlines() |
          select("match", ".*solr.*.tgz$") | list }}'

    - name: check for downloaded solr
      become: false
      stat:
        path: "{{ local_tmp_directory }}/solr-{{ solr_version }}.tgz"
      register: _downloaded_solr

    - name: compute solr checksum
      become: false
      stat:
        path: "{{ local_tmp_directory }}/solr-{{ solr_version }}.tgz"
        checksum_algorithm: sha512
      delegate_to: localhost
      register: _downloaded_solr_checksum
      when: _downloaded_solr is defined and _downloaded_solr.stat.exists

    - name: download application archiv solr-{{ solr_version }}.tgz to local directory {{ local_tmp_directory }}
      become: false
      get_url:
        url: "{{ solr_download_url }}"
        dest: "{{ local_tmp_directory }}"
        checksum: 'sha512:{{ solr_checksum }}'
      register: _download_artefact
      until: _download_artefact is succeeded
      retries: 5
      delay: 2
      check_mode: false
      delegate_to: localhost
      when: (
        (_downloaded_solr is defined and not _downloaded_solr.stat.exists) or
        (solr_checksum != _downloaded_solr_checksum.stat.checksum) )

    - name: deploy application archiv
      copy:
        src: "{{ local_tmp_directory }}/solr-{{ solr_version }}.tgz"
        dest: "{{ deployment_tmp_directory }}"

    - name: unpack solr binary
      unarchive:
        src: "{{ deployment_tmp_directory }}/solr-{{ solr_version }}.tgz"
        dest: /opt/
        owner: "{{ solr_user }}"
        group: "{{ solr_group }}"
        unsafe_writes: true
        mode: 0750
        copy: false

    - name: Remove docs, if not needed.
      file:
        path: "{{ solr_application_directory }}/{{ item }}"
        state: absent
      loop:
        - docs
        - licenses
      when: solr_remove_cruft

    - name: Remove example dir, if not needed.
      file:
        path: "{{ solr_application_directory }}/example"
        state: absent
      when:
        - solr_remove_cruft
        - solr_major_version >= '5'

    - name: get system timestamp  # noqa 305
      shell: 'date +"%Y-%m-%d %H-%M-%S %Z"'
      register: timestamp
      no_log: true
      tags:
        - skip_ansible_lint

    - name: set facts
      set_fact:
        current_date: "{{ timestamp.stdout[0:10] }}"
        current_time: "{{ timestamp.stdout[11:] }}"
        current_timestamp: "{{ timestamp.stdout }}"
        solr_version: "{{ solr_version }}"
      no_log: true

    - name: create custom fact file
      template:
        src: application.fact.j2
        dest: /etc/ansible/facts.d/solr.fact
        owner: root
        group: root
        mode: 0755

    - name: do facts module to get latest information
      setup:

  when: _solr_unpack is defined and not _solr_unpack.stat.exists
  tags:
    - solr

- name: create current link for solr
  file:
    src: "/opt/solr-{{ solr_version }}"
    dest: /opt/solr
    state: link
    force: true
    follow: false
    owner: "{{ solr_user }}"
    group: "{{ solr_group }}"
    mode: 0750

- name: create systemd service unit
  template:
    src: init/systemd/solr.service.j2
    dest: "{{ systemd_lib_directory }}/solr.service"
    owner: root
    group: root
    mode: 0644
  notify: reload systemctl daemon
