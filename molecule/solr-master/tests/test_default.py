import pytest
import os
import yaml

#import requests
#from requests.exceptions import ConnectionError, Timeout

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/opt/solr",
    "/var/log/solr",
    "/opt/coremedia/solr-home",
    "/var/coremedia/solr-data",
    "/run/solr",
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/solr.fact",
    "/etc/default/solr.in.sh",
    "/opt/coremedia/solr-home/solr.xml"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("solr").exists
    assert host.group("coremedia").exists


def test_solr_service(host):
    service = host.service("solr")

    if( service.__class__.__name__ != 'SysvService' ):
        assert service.is_enabled == True

    assert service.is_running == True


def test_solr_master_enabled(host):
    assert host.file("/etc/default/solr.in.sh").contains("solr.master=true")


def test_solr_slave_disabled(host):
    assert host.file("/etc/default/solr.in.sh").contains("solr.slave=false")


def test_solr_xml(host):

    config_file = "/opt/coremedia/solr-home/solr.xml"
    content = host.file(config_file).content_string

    assert 'str name="sharedLib"' in content
    assert 'bool name="shareSchema"' in content
    assert '<str name="coreRootDirectory">${coreRootDirectory:cores}</str>' in content


@pytest.mark.parametrize("ports", [
    '127.0.0.1:40080',
    '0.0.0.0:40099',
    '127.0.0.1:39080'
])
def test_open_port(host, ports):
#    for i in host.socket.get_listening_sockets():
#        print( i )
    solr = host.socket("tcp://{}".format(ports))
    assert solr.is_listening


# def test_requests_get(host):
#
#     url = "http://{}:40080/solr/admin/cores?action=STATUS&wt=json".format('127.0.0.1')
#     response = None
#
#     try:
#         response = requests.get(
#                 url = url,
#                 timeout = (1, 3),
#                 allow_redirects = False,
#             )
#         print(response)
#         print(response.status_code)
#
#     except Timeout:
#         print('The request timed out')
#         assert False
#     except ConnectionRefusedError as e:
#         print('Failed to establish a new connection: {} '.format(url) )
#         assert False
#     except ConnectionError as e:
#         print('error fetching data from {}' . format(url))
#         assert False
#
#     assert True
